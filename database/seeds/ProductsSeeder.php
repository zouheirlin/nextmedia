<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);

        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);

        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);

        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);
        
        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);

        DB::table('products')->insert([
            'title' => Str::random(10),
            'description' => Str::random(30),
            'price' => rand(5,50),
            'inventory' => rand(3,20),
        ]);

    }
}
