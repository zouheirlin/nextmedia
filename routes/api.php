<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'seller'], function($router) {
    Route::resource('/products', 'ProductsController');
    Route::get('/orders', 'OrdersController@getOrders');
    Route::get('/orders/{id}', 'OrdersController@showOrder');
    Route::get('/orders/confirm/{id}', 'OrdersController@confirmOrder');
    Route::get('/orders/close/{id}', 'OrdersController@closeOrder');
});

Route::group(['prefix' => 'store'], function($router) {
    Route::get('/products', 'CustomerController@getProducts');
    Route::post('/register', 'CustomerController@register');
    Route::post('/cart/add', 'CustomerController@addProduct');
    Route::post('/order', 'CustomerController@submitOrder');
});