<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['name', 'address', 'country'];

    public function orders(){
        return $this->hasMany('App\Order');
    }

    public function products(){
        return $this->belongsToMany('App\Product', 'cart_products', 'product_id', 'customer_id');
    }
}
