<?php 

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;

class SellerService {

    protected $product;
    protected $order;

    public function __construct(ProductRepository $product,OrderRepository $order){
		$this->product = $product;
		$this->order = $order;
	}
 
	public function allProducts(){
		return $this->product->all();
    }
    
    public function allOrders(){
        return $this->order->all();
    }
 
    public function createProduct($data){         
        return $this->product->create($data);
    }
 
	public function getProduct($id){
        return $this->product->find($id);
    }
    
    public function getOrderWithProducts($id){
        return $this->order->with('products')->find($id);
    }
 
	public function updateProduct($data, $id){
        return $this->product->update($data, $id);
	}
 
	public function deleteProduct($id){
        return $this->product->delete($id);
    }

    public function confirmOrder($id){
        $order = $this->order->find($id);
        if($order){
            if($order->status == "confirm"){
                return "Order already confirmed";
            }else{
                $order->status = "confirm";
                $order->save();
                return "Order status confirmed";
            }
        }
        return "Order not found";
    }

    public function closeOrder($id){
        $order = $this->order->find($id);
        if($order){
            if($order->status == "close"){
                return "Order already closed";
            }else{
                $order->status = "close";
                $order->save();
                return "Order status closed";
            }
        }
        return "Order not found";
    }
}