<?php 

namespace App\Services;

use App\Repositories\ProductRepository;
use App\Repositories\OrderRepository;
use App\Repositories\CustomerRepository;

class CustomerService {

    protected $product;
    protected $order;
    protected $customer;

    public function __construct(ProductRepository $product,OrderRepository $order, CustomerRepository $customer){
		$this->product = $product;
		$this->order = $order;
		$this->customer = $customer;
	}
 
	public function allProductsWithStock(){
		return $this->product->setWhere('inventory','>',0)->all();
    }

    public function productWithStock($id){
        return $this->product->setWhere('inventory','>',0)->find($id);
    }
 
    public function createProduct(Request $request){         
        return $this->product->create($request->all());
    }
    
    public function registerCustomer($data){
        return $this->customer->create($data);
    }

    public function orderCreateAndAddProducts($id){
        $data = ['customer_id' => $id, 'status' => 'pending'];
        $products = $this->customer->find($id)->products;
        $total = 0;
        $order = $this->order->create($data);
        foreach($products as $product){
            if($product->inventory > 0){
                $order->products()->attach($product->id);
                $product->inventory = $product->inventory - 1;
                $product->save();
                $total = $total + $product->price;
            }
        }
        $order->total = $total;
        $order->save();
        return $order;
    }

    public function addProduct($customerId, $productId){
        $customer = $this->customer->find($customerId);
        $product = $this->productWithStock($productId);
        if($product){
            if($customer->products->contains($product->id)){
                return "Product already in your cart";
            }
            $customer->products()->attach($product->id);
            return "Product added to your cart";
        }
        return "Product is out of stock";
    }

}