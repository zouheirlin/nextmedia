<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = ['customer_id','status'];

    public function products(){
        return $this->belongsToMany('App\Product', 'order_products','order_id','product_id');
    }

    public function customer(){
        return $this->belongsTo('App\Customer');
    }

}
