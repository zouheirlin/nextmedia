<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SellerService;

class ProductsController extends Controller
{

    protected $seller;

    public function __construct(SellerService $seller){
        $this->middleware('basicAuth');
        $this->seller = $seller;
    }

    public function index()
    {
        $products = $this->seller->allProducts();
        return response()->json(['products' => $products]);
    }

    public function store(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'inventory' => 'required',
        ]);
        $product = $this->seller->createProduct($request->all());
        return response()->json(['product' => $product, 'message' => "Product created successfully"], 200);
    }

    public function show($id)
    {
        $product = $this->seller->getProduct($id);
        return response()->json(['product' => $product], 200);
    }

    public function update(Request $request)
    {
        $validateData = $request->validate([
            'title' => 'required',
            'description' => 'required',
            'price' => 'required',
            'inventory' => 'required',
        ]);
        $this->seller->updateProduct($request->all(),$request->id);
        return response()->json(['message' => "Product updated successfully"], 200);
    }

    public function destroy($id)
    {
        $product = $this->seller->deleteProduct($id);
        return response()->json(['message' => "Product deleted successfully"], 200);
    }
}
