<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SellerService;

class OrdersController extends Controller
{
    protected $seller;

    function __construct(SellerService $seller){
        $this->middleware('basicAuth');
        $this->seller = $seller;
    }

    function getOrders(){
        $orders = $this->seller->allOrders();
        return response()->json(['orders' => $orders], 200);
    }

    function showOrder($id){
        $order = $this->seller->getOrderWithProducts($id);
        return response()->json(['order' => $order], 200);
    }

    function confirmOrder($id){
        $res = $this->seller->confirmOrder($id);
        return response()->json(['message' => $res], 200);
    }

    function closeOrder($id){
        $res = $this->seller->closeOrder($id);
        return response()->json(['message' => $res], 200);
    }
}
