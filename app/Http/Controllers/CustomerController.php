<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\Customer;
use Illuminate\Support\Facades\Input;
use App\Services\CustomerService;

class CustomerController extends Controller
{
    protected $customer;


    function __construct(CustomerService $customer){
        $this->customer = $customer;
    }

    public function getProducts(){
        $products = $this->customer->allProductsWithStock();
        if($products)
            return response()->json(['products' => $products], 200);
    }

    public function register(Request $request){
        $validateData = $request->validate([
            'name' => 'required',
            'address' => 'required',
            'country' => 'required',
        ]);

        $customer = $this->customer->registerCustomer($request->all());
        return response()->json(['message' => "Customer added successfully"], 200);
    }

    public function addProduct(Request $request){
        $message = $this->customer->addProduct($request->customer, $request->product);
        return response()->json(['message' => $message], 200);
    }

    public function submitOrder(Request $request){
        $order = $this->customer->orderCreateAndAddProducts($request->customer);
        if(sizeof($order->products) > 0){
            return response()->json(['message' => "Order created successfully"], 200);
        }
        return response()->json(['message' => "Products in your cart are out of stock"], 404);
    }

}
