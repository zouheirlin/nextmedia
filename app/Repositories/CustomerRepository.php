<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class CustomerRepository extends Repository {

    function model()
    {
        return 'App\Customer';
    }

}