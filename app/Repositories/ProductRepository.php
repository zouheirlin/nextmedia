<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class ProductRepository extends Repository {

    function model()
    {
        return 'App\Product';
    }

}