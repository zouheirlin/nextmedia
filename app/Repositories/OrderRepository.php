<?php 
namespace App\Repositories;

use App\Repositories\Repository;

class OrderRepository extends Repository {

    function model()
    {
        return 'App\Order';
    }

}