<?php

namespace App\Repositories;

use App\Repositories\RepositoryInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Container\Container as App;

abstract class Repository implements RepositoryInterface
{
    private $app;

    protected $model;
    protected $with;
    protected $where;

    public function __construct(App $app)
    {
        $this->app = $app;
        $this->makeModel();
    }

    abstract function model();

    public function makeModel() {
        $model = $this->app->make($this->model());
        return $this->model = $model->newQuery();
    }

    public function all()
    {   
        $this->eagerLoadRelations();
        $this->whereConditions();
        return $this->model->get(['*']);
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id, $attribute='id')
    {
        return $this->model->where($attribute, '=', $id)->update($data);
    }

    public function delete($id)
    {
        return $this->model->find($id)->delete();
    }

    public function find($id)
    {
        $this->eagerLoadRelations();
        $this->whereConditions();
        return $this->model->find($id);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function with($relations) {
        if (is_string($relations)) $relations = func_get_args();

        $this->with = $relations;

        return $this;
    }

    protected function eagerLoadRelations() {
        if(!is_null($this->with)) {
            foreach ($this->with as $relation) {
                $this->model->with($relation);
            }
        }
        return $this;
    }
    
    
    public function setWhere($attribute, $condition, $value){
        $this->where = ['attribute' => $attribute, 'condition' => $condition, 'value' => $value];
        return $this;
    }

    protected function whereConditions(){
        if(!is_null($this->where)){
            $this->model->where($this->where['attribute'], $this->where['condition'], $this->where['value']);
        }
        return $this;
    }

    public function orderCreateAndAddProducts($data, $products){
        $total = 0;
        $order = $this->model->create($data);
        foreach($products as $product){
            if($product->inventory > 0){
                $order->products()->attach($product->id);
                $product->inventory = $product->inventory - 1;
                $product->save();
                $total = $total + $product->price;
            }
        }
        $order->total = $total;
        $order->save();
        return $order;
    }

    public function confirmOrder($id){
        $order = $this->model->find($id);
        if($order){
            if($order->status == "confirm"){
                return "Order already confirmed";
            }else{
                $order->status = "confirm";
                $order->save();
                return "Order status confirmed";
            }
        }
        return "Order not found";
    }

    public function closeOrder($id){
        $order = $this->model->find($id);
        if($order){
            if($order->status == "close"){
                return "Order already closed";
            }else{
                $order->status = "close";
                $order->save();
                return "Order status closed";
            }
        }
        return "Order not found";
    }

}