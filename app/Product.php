<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['title', 'description', 'price', 'inventory'];

    public function orders(){
        return $this->belongsToMany('App\Order', 'order_products', 'product_id','order_id');
    }

    public function customers(){
        return $this->belongsToMany('App\Customer', 'cart_products', 'customer_id', 'product_id');
    }
}
